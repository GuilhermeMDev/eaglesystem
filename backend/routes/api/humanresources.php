<?php

declare(strict_types=1);

 use App\Http\Controllers\Humanresources\CollaboratorController;
use Devitools\Http\Routing\Router;

Router::restricted()->group(static function () {
  // example:
   Router::provide('/humanresources/collaborator', CollaboratorController::class);
});
