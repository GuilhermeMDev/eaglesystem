<?php

declare(strict_types=1);

 use App\Http\Controllers\Registration\ProductController;
use Devitools\Http\Routing\Router;

Router::restricted()->group(static function () {
  // example:
   Router::provide('/registration/product', ProductController::class);
});
