<?php

declare(strict_types=1);

use App\Http\Controllers\Inventory\ProviderController;
use Devitools\Http\Routing\Router;

Router::restricted()->group(static function () {

   Router::provide('/inventory/provider', ProviderController::class);
});
