<?php

declare(strict_types=1);

 use App\Http\Controllers\Marketing\ProjectController;
use Devitools\Http\Routing\Router;

Router::restricted()->group(static function () {
  // example:
   Router::provide('/marketing/project', ProjectController::class);
});
