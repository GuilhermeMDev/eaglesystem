<?php

declare(strict_types=1);

use Devitools\Database\Migration\TableCreate;
use Devitools\Database\Table;

/**
 * Class ProvidersCreate
 */
class ProvidersCreate extends TableCreate
{
    /**
     * @return string
     */
    protected function table(): string
    {
        return 'providers';
    }

    /**
     * @param Table $table
     */
    protected function withStatements(Table $table): void
    {

        $table->string('name');
        $table->string('cnpj');
    }
}
