version: '3.7'

# Networks
networks:
  # Internal network
  internal:
    driver: bridge

# Volumes
volumes:
  # MySQL volume
  eagle-mysql_data:
    driver: local
  # Redis volume
  eagle-redis_data:
    driver: local
  # Minio data
  eagle-minio_data:
    driver: local
  # Minio config
  eagle-minio_config:
    driver: local

# Services
services:

  # Nginx
  eagle-nginx:
    image: webdevops/php-nginx-dev:7.4
    container_name: eagle-nginx
    networks:
      - internal
    working_dir: /var/www/app
    volumes:
      - .:/var/www/app
      - .docker/nginx/opt/docker/etc/nginx/vhost.common.d/custom.conf:/opt/docker/etc/nginx/vhost.common.d/custom.conf
      - .docker/nginx/usr/local/etc/php/conf.d/php.ini:/usr/local/etc/php/conf.d/php.ini
    depends_on:
      - eagle-mysql
      - eagle-redis
    environment:
      - WEB_DOCUMENT_ROOT=/var/www/app/public
      - PHP_DISPLAY_ERRORS=0
      - PHP_MEMORY_LIMIT=2048M
      - PHP_MAX_EXECUTION_TIME=300
      - PHP_POST_MAX_SIZE=500M
      - PHP_UPLOAD_MAX_FILESIZE=500M
      - PHP_DEBUGGER="xdebug"
      #      - PHP_IDE_CONFIG="serverName=_"
      - XDEBUG_REMOTE_AUTOSTART=On
      - XDEBUG_REMOTE_CONNECT_BACK=Off
      - XDEBUG_REMOTE_HOST=host.docker.internal
      - XDEBUG_REMOTE_PORT=9090
      - XDEBUG_PROFILER_ENABLE=Off
      - PHP_DATE_TIMEZONE=UTC
    expose:
      - 9090
    ports:
      - 8080:80

  # MySQL
  eagle-mysql:
    image: mysql:5.7
    container_name: eagle-mysql
    networks:
      - internal
    working_dir: /var/www/app
    volumes:
      - eagle-mysql_data:/var/lib/mysql
      - .:/var/www/app
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=database
      - MYSQL_USER=user
      - MYSQL_PASSWORD=password
    ports:
      - 3306:3306

  # Redis
  eagle-redis:
    image: redis:4.0
    container_name: eagle-redis
    command: --appendonly yes
    networks:
      - internal
    volumes:
      - eagle-redis_data:/data
    ports:
      - 6379:6379

  # Queue SupervisorD
  eagle-queue:
    image: webdevops/php-nginx:7.4
    container_name: eagle-queue
    working_dir: /var/www/app
    networks:
      - internal
    volumes:
      - .:/var/www/app
      - .docker/queue/opt/docker/etc/supervisor.d/php-fpm.conf:/opt/docker/etc/supervisor.d/php-fpm.conf
    depends_on:
      - eagle-mysql
      - eagle-redis

  # Min.io
  eagle-minio:
    image: minio/minio:RELEASE.2021-08-05T22-01-19Z
    container_name: eagle-minio
    entrypoint: sh
    command: -c 'mkdir -p /data/static && minio server /data'
    networks:
      - internal
    volumes:
      - eagle-minio_data:/data
      - eagle-minio_config:/root/.minio
    ports:
      - 8090:9000
    environment:
      - MINIO_ROOT_USER=s20B73VI0E0164ABPXZIH
      - MINIO_ROOT_PASSWORD=M5mTytfy7U4xsxUitNWVJjC0AvV3FwXvXZQmYXab
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost:9000/minio/health/live" ]
      interval: 30s
      timeout: 20s
      retries: 3
