<?php

declare(strict_types=1);

namespace App\Domains\Inventory;

use Devitools\Agnostic\Schema;

/**
 * Class Provider
 *
 * @package App\Domains\Inventory
 */
class Provider extends Schema
{
    /**
     * The resource associated with the schema.
     *
     * @return string
     */
    public static function resource(): string
    {
        return 'providers';
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return 'inventory/provider';
    }

    /**
     * Build the schema fields and actions.
     *
     * @return void
     */
    public function construct(): void
    {
        $this->addField('name')
            ->validationRequired();

        $this->addField('cnpj')
            ->validationRequired();
    }
}
