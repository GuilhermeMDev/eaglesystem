<?php

declare(strict_types=1);

namespace App\Domains\Inventory\Provider;

use Devitools\Persistence\AbstractRepository;
use App\Domains\Inventory\Provider;

/**
 * Class ProviderRepository
 *
 * @package App\Domains\Inventory\Provider
 */
class ProviderRepository extends AbstractRepository
{
    /**
     * The entity class name used in repository
     *
     * @var string
     */
    protected string $prototype = Provider::class;

    /**
     * @return array
     */
    public function getFilterable(): array
    {
        return ['uuid', 'name'];
    }
}
