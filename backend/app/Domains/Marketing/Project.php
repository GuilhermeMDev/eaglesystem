<?php

declare(strict_types=1);

namespace App\Domains\Marketing;

use Devitools\Agnostic\Schema;

/**
 * Class Project
 *
 * @package App\Domains\Marketing
 */
class Project extends Schema
{
    /**
     * The resource associated with the schema.
     *
     * @return string
     */
    public static function resource(): string
    {
        return 'projects';
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return 'marketing/project';
    }

    /**
     * Build the schema fields and actions.
     *
     * @return void
     */
    public function construct(): void
    {
        $this->addField('name')
            ->validationRequired();
    }
}
