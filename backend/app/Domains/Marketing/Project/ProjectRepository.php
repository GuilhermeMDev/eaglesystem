<?php

declare(strict_types=1);

namespace App\Domains\Marketing\Project;

use Devitools\Persistence\AbstractRepository;
use App\Domains\Marketing\Project;

/**
 * Class ProjectRepository
 *
 * @package App\Domains\Marketing\Project
 */
class ProjectRepository extends AbstractRepository
{
    /**
     * The entity class name used in repository
     *
     * @var string
     */
    protected string $prototype = Project::class;

    /**
     * @return array
     */
    public function getFilterable(): array
    {
        return ['uuid', 'name'];
    }
}
