<?php

declare(strict_types=1);

namespace App\Domains\Humanresources;

use Devitools\Agnostic\Schema;

/**
 * Class Collaborator
 *
 * @package App\Domains\Humanresources
 */
class Collaborator extends Schema
{
    /**
     * The resource associated with the schema.
     *
     * @return string
     */
    public static function resource(): string
    {
        return 'collaborators';
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return 'humanresources/collaborator';
    }

    /**
     * Build the schema fields and actions.
     *
     * @return void
     */
    public function construct(): void
    {
        $this->addField('name')
            ->validationRequired();

        $this->addField('familyName')
            ->validationRequired();

        $this->addField('cpf')
            ->isInputPlan()
            ->validationRequired();

        $this.$this->addField('phoneNumber')
            ->validationRequired();

        $this->addField('notes')
            ->isText();
    }

}
