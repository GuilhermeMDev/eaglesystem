<?php

declare(strict_types=1);

namespace App\Domains\Humanresources\Collaborator;

use Devitools\Persistence\AbstractRepository;
use App\Domains\Humanresources\Collaborator;

/**
 * Class CollaboratorRepository
 *
 * @package App\Domains\Humanresources\Collaborator
 */
class CollaboratorRepository extends AbstractRepository
{
    /**
     * The entity class name used in repository
     *
     * @var string
     */
    protected string $prototype = Collaborator::class;

    /**
     * @return array
     */
    public function getFilterable(): array
    {
        return ['uuid', 'name'];
    }
}
