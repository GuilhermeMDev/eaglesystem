<?php

declare(strict_types=1);

namespace App\Domains\Registration\Product;

use Devitools\Persistence\AbstractRepository;
use App\Domains\Registration\Product;

/**
 * Class ProductRepository
 *
 * @package App\Domains\Registration\Product
 */
class ProductRepository extends AbstractRepository
{
    /**
     * The entity class name used in repository
     *
     * @var string
     */
    protected string $prototype = Product::class;

    /**
     * @return array
     */
    public function getFilterable(): array
    {
        return ['uuid', 'name'];
    }
}
