<?php

declare(strict_types=1);

namespace App\Domains\Registration;

use App\Domains\Inventory\Provider;
use Devitools\Agnostic\Schema;

/**
 * Class Product
 *
 * @package App\Domains\Registration
 */
class Product extends Schema
{
    /**
     * The resource associated with the schema.
     *
     * @return string
     */
    public static function resource(): string
    {
        return 'products';
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return 'registration/product';
    }

    /**
     * Build the schema fields and actions.
     *
     * @return void
     */
    public function construct(): void
    {
        $this->addField('name')
            ->validationRequired();

        $this->addField('price')
         ->isCurrency()
         ->validationRequired();

        $this->addField('providerId')
            ->isSelectRemote(Provider::class, 'provider')
            ->validationRequired();
    }
}
