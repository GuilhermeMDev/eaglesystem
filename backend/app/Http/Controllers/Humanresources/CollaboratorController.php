<?php

declare(strict_types=1);

namespace App\Http\Controllers\Humanresources;

use Devitools\Http\Controllers\AbstractRestController;
use App\Domains\Humanresources\Collaborator\CollaboratorRepository;

/**
 * Class CollaboratorController
 *
 * @package App\Http\Controllers\Humanresources
 */
class CollaboratorController extends AbstractRestController
{
    /**
     * CollaboratorController constructor.
     *
     * @param CollaboratorRepository $repository
     */
    public function __construct(CollaboratorRepository $repository)
    {
        parent::__construct($repository);
    }
}
