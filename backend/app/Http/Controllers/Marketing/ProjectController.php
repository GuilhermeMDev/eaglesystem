<?php

declare(strict_types=1);

namespace App\Http\Controllers\Marketing;

use Devitools\Http\Controllers\AbstractRestController;
use App\Domains\Marketing\Project\ProjectRepository;

/**
 * Class ProjectController
 *
 * @package App\Http\Controllers\Marketing
 */
class ProjectController extends AbstractRestController
{
    /**
     * ProjectController constructor.
     *
     * @param ProjectRepository $repository
     */
    public function __construct(ProjectRepository $repository)
    {
        parent::__construct($repository);
    }
}
