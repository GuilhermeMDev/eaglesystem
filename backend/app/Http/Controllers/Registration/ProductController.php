<?php

declare(strict_types=1);

namespace App\Http\Controllers\Registration;

use Devitools\Http\Controllers\AbstractRestController;
use App\Domains\Registration\Product\ProductRepository;

/**
 * Class ProductController
 *
 * @package App\Http\Controllers\Registration
 */
class ProductController extends AbstractRestController
{
    /**
     * ProductController constructor.
     *
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        parent::__construct($repository);
    }
}
