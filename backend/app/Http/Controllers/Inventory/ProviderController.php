<?php

declare(strict_types=1);

namespace App\Http\Controllers\Inventory;

use Devitools\Http\Controllers\AbstractRestController;
use App\Domains\Inventory\Provider\ProviderRepository;

/**
 * Class ProviderController
 *
 * @package App\Http\Controllers\Inventory
 */
class ProviderController extends AbstractRestController
{
    /**
     * ProviderController constructor.
     *
     * @param ProviderRepository $repository
     */
    public function __construct(ProviderRepository $repository)
    {
        parent::__construct($repository);
    }
}
