import { action } from '@devitools/Security/actions'
export { domain, icon } from 'resources/views/dashboard/humanresources'
// add children
import * as collaborator from 'resources/views/dashboard/humanresources/collaborator'

/** @type {*[]} */
export const children = [
  action(collaborator)
]
