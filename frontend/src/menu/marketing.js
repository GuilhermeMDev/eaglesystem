import { action } from '@devitools/Security/actions'
export { domain, icon } from 'resources/views/dashboard/marketing'
// add children
import * as project from 'resources/views/dashboard/marketing/project'

/** @type {*[]} */
export const children = [
  action(project)
]
