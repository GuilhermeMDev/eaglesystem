import { action } from '@devitools/Security/actions'
export { domain, icon } from 'resources/views/dashboard/inventory'
// add children
import * as provider from 'resources/views/dashboard/inventory/provider'

/** @type {*[]} */
export const children = [
  action(provider)
]
