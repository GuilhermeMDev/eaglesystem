import { action } from '@devitools/Security/actions'
export { domain, icon } from 'resources/views/dashboard/registration'
// add children
import * as product from 'resources/views/dashboard/registration/product'

/** @type {*[]} */
export const children = [
  action(product)
]
