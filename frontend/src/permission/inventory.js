import { permission } from '@devitools/Security/permissions'
export { domain, icon } from 'resources/views/dashboard/inventory'
// add children
import * as provider from 'resources/views/dashboard/inventory/provider'

/** @type {*[]} */
export const children = [
  permission(provider)
]
