import { permission } from '@devitools/Security/permissions'
export { domain, icon } from 'resources/views/dashboard/humanresources'
// add children
import * as collaborator from 'resources/views/dashboard/humanresources/collaborator'

/** @type {*[]} */
export const children = [
  permission(collaborator)
]
