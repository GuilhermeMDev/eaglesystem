import { actionEntry, actionGroup } from '@devitools/Security/actions'
import { getAllowedMenuEntries } from 'src/settings/security'

import * as marketing from 'src/menu/marketing'
import * as humanresources from 'src/menu/humanresources'
import * as inventory from 'src/menu/inventory'
import * as registration from 'src/menu/registration'
import * as admin from 'src/menu/admin'

/**
 * @param {Record<string, unknown>|undefined} session
 * @return {*[]}
 */
export default function (session) {
  if (!session) {
    return []
  }

  const { permissions } = session
  if (!Array.isArray(permissions)) {
    return [
      actionEntry('home', '/dashboard/home', 'home', 'home')
    ]
  }

  const actions = [
    actionEntry('home', '/dashboard/home', 'home', 'home'),
    actionGroup(marketing),
    actionGroup(humanresources),
    actionGroup(inventory),
    actionGroup(registration),
    actionGroup(admin)
  ]

  return getAllowedMenuEntries(actions)
}
