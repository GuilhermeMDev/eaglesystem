import { permissionGroup } from '@devitools/Security/permissions'

import * as marketing from 'src/permission/marketing'
import * as humanresources from 'src/permission/humanresources'
import * as inventory from 'src/permission/inventory'
import * as registration from 'src/permission/registration'
import * as admin from 'src/permission/admin'

/**
 * @type {*}
 */
export const permissions = [
  permissionGroup('all', 'ballot', [
    permissionGroup(marketing),
    permissionGroup(humanresources),
    permissionGroup(inventory),
    permissionGroup(registration),
    permissionGroup(admin)
  ])
]

/**
 * @return {string[]}
 */
export function groups () {
  try {
    const root = permissions[0]
    const children = root.children.map((group) => group.namespace)
    return [root.namespace, ...children]
  } catch (e) {
    // silence is gold
  }
  return []
}

/**
 * @param {string} namespace
 * @return {string[]}
 */
export function dependencies (namespace) {
  const references = {}
  return references[namespace] || []
}

/**
 * @return {*}
 */
export function namespaces () {
  const reducer = (accumulator, permission) => {
    if (permission.children) {
      return permission.children.reduce(reducer, accumulator)
    }
    if (!permission.namespace) {
      return accumulator
    }
    accumulator.push(permission.namespace)
    return accumulator
  }
  return permissions.reduce(reducer, [])
}

/**
 * @return {Array}
 */
export default function () {
  return permissions
}
