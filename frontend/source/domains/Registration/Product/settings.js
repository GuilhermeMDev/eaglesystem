/** @type {string} */
export const domain = 'registration/product'

/** @type {string} */
export const resource = '/registration/product'
