import Schema from '@devitools/Agnostic/Schema'

import Service from './ProductService'
import { domain } from '../settings'
import ProviderSchema from "app/source/domains/Inventory/Provider/Schema/ProviderSchema";

/**
 * @class { ProductSchema }
 */
export default class ProductSchema extends Schema {
  /**
   * @type {string}
   */
  static domain = domain

  /**
   * @type {Http}
   */
  service = Service

  /**
   */
  construct () {
    this.addField('name')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormAutofocus()
      .fieldFormWidth(100)
      .fieldFormFill('name')
      .validationRequired()


    this.addField('price')
      .fieldIsCurrency()
      .fieldTableShow()
      .fieldFormWidth(40)

    this.addField('provider')
      .fieldIsSelectRemote(ProviderSchema.provideRemote())
      .fieldTableShow()
      .fieldFormFill()
      .fieldFormWidth(60)
  }
}
