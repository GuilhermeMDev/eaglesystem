import Rest from '@devitools/Services/Rest'
import { resource } from '../settings'

/**
 * @type { ProductService }
 */
export default class ProductService extends Rest {
  /**
   * @type {String}
   */
  resource = resource
}
