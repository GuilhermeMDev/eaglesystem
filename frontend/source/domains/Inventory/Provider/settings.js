/** @type {string} */
export const domain = 'inventory/provider'

/** @type {string} */
export const resource = '/inventory/provider'
