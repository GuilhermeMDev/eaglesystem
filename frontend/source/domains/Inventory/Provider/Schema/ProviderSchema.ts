import Schema from '@devitools/Agnostic/Schema'

import Service from './ProviderService'
import { domain } from '../settings'

/**
 * @class { ProviderSchema }
 */
export default class ProviderSchema extends Schema {
  /**
   * @type {string}
   */
  static domain = domain

  /**
   * @type {Http}
   */
  service = Service

  /**
   */
  construct () {
    this.addField('name')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormAutofocus()
      .fieldFormWidth(80)
      .fieldFormFill('name')
      .validationRequired()

    this.addField('cnpj')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldFormWidth(40)
      .fieldAsMasked('##.###.###/####-##')
  }
}
