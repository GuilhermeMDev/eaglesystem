import Rest from '@devitools/Services/Rest'
import { resource } from '../settings'

/**
 * @type { ProviderService }
 */
export default class ProviderService extends Rest {
  /**
   * @type {String}
   */
  resource = resource
}
