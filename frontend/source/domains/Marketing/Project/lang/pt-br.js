import { routes } from 'resources/lang/pt-br/helper'
import parent from 'resources/lang/pt-br/menu/marketing'

const plural = 'Projetos'
const singular = 'Projeto'

export const print = {
  title: `Imprimir ${singular}`
}

export const groups = {
  register: 'CADASTRO',
  historic: 'Histórico'
}

export const fields = {
  name: {
    label: 'Nome',
    tooltip: 'ex.: Nome usado para representar o registro',
    placeholder: 'ex: Digite um nome bem legal'
  },
  year: {
    label: 'Ano'
  }
}

/*
export const validations = {
  name: {
    requiredIf: 'ex.: Nome é obrigatório'
  }
}
*/

export default {
  routes: routes(parent, plural, singular),
  groups,
  fields,
  // validations
  print
}
