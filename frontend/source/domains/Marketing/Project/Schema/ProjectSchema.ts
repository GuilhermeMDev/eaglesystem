import Schema from '@devitools/Agnostic/Schema'

import Service from './ProjectService'
import { domain } from '../settings'

/**
 * @class { ProjectSchema }
 */
export default class ProjectSchema extends Schema {
  /**
   * @type {string}
   */
  static domain = domain

  /**
   * @type {Http}
   */
  service = Service

  static groupType = 'tabs'
  /**
   */
  construct () {
    this.addGroup('register')

    this.addField('name')
      .fieldGroup('register')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormAutofocus()
      .fieldFormWidth(100)
      .fieldFormFill('name')
      .validationRequired()

    this.addGroup('historic')

    this.addField('year')
      .fieldGroup('historic')
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormWidth(100)
      .fieldIsSelect()
  }
}
