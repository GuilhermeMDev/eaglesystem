/** @type {string} */
export const domain = 'marketing/project'

/** @type {string} */
export const resource = '/marketing/project'
