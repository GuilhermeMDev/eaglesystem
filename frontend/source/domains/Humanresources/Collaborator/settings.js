/** @type {string} */
export const domain = 'humanresources/collaborator'

/** @type {string} */
export const resource = '/humanresources/collaborator'
