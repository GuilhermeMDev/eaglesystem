import { routes } from 'resources/lang/pt-br/helper'
import parent from 'resources/lang/pt-br/menu/humanresources'

const plural = 'Colaboradores'
const singular = 'Colaborador'

export const print = {
  title: `Imprimir ${singular}`
}

export const fields = {
  name: {
    label: 'Nome',
    tooltip: 'ex.: Nome usado para representar o registro',
    placeholder: 'ex: Digite nome e sobrenome'
  },

  familyName: {
    label: 'Sobrenome',
    tooltip: 'Informe o Sobrenome do colaborador'
  },

  cpf: {
    label: 'CPF',
    tooltip: 'Documento usado para representar o registro'
  },

  phoneNumber: {
    label: 'Celular'
  },
  notes: {
    label: 'Descrição',
    tooltip: 'Descreva o básico sobre o colaborador'
  }
}

/*
export const validations = {
  name: {
    requiredIf: 'ex.: Nome é obrigatório'
  }
}
*/

export const groups = {
  main: 'CADASTRO'
}

/*
export const actions = {
  home: {
    label: plural
  }
}
*/

export default {
  routes: routes(parent, plural, singular),
  groups,
  fields,
  // validations
  print
}
