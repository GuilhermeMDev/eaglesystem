import Rest from '@devitools/Services/Rest'
import { resource } from '../settings'

/**
 * @type { CollaboratorService }
 */
export default class CollaboratorService extends Rest {
  /**
   * @type {String}
   */
  resource = resource
}
