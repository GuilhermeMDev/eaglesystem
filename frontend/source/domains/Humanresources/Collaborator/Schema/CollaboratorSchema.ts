import Schema from '@devitools/Agnostic/Schema'

import Service from './CollaboratorService'
import { domain } from '../settings'

/**
 * @class { CollaboratorSchema }
 */
export default class CollaboratorSchema extends Schema {
  /**
   * @type {string}
   */
  static domain = domain

  /**
   * @type {Http}
   */
  service = Service

  /**
   * @type {String}
   */
  static groupType = 'tabs'

  construct () {
    this.addGroup('main')

    this.mainFields('main')
  }

  /**
   * @param {string} group
   * @return {void}
   */

  mainFields (group: string) {
    this.addField('name')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormAutofocus()
      .fieldFormWidth(40)
      .fieldFormFill('name')
      .validationRequired()
      .fieldGroup('main')

    this.addField('familyName')
      .fieldIsInputPlan()
      .fieldFormWidth(40)
      .fieldTableShow()
      .fieldFormFill('name')
      .validationRequired()
      .fieldGroup('main')

    this.addField('cpf')
      .fieldIsInputPlan()
      .fieldTableShow()
      .fieldTableWhere()
      .fieldFormWidth(25)
      .validationRequired()
      .fieldAsMasked('###.###.###-##')
      .fieldGroup('main')

    this.addField('phoneNumber')
      .fieldAsPhone('(##) #####-####')
      .fieldFormWidth(25)
      .fieldTableShow()
      .fieldGroup('main')

    this.addField('notes')
      .fieldIsTextEditor()
      .fieldFormWidth(80)
      .fieldTableShow()
      .fieldFormFill('paragraph')
      .fieldGroup('main')

  }
}
