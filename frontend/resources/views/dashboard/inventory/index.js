/** @type {string} */
export { domain } from 'source/domains/Inventory/settings'

/** @type {string} */
export const icon = 'inventory'
