export { domain } from 'source/domains/Inventory/Provider/settings'

/** @type {string} */
export const path = '/dashboard/inventory/provider'

/** @type {string} */
export const icon = 'contact_page'

/** @type {function} */
export const table = () => import('resources/views/dashboard/inventory/provider/ProviderTable.vue')

/** @type {function} */
export const form = () => import('resources/views/dashboard/inventory/provider/ProviderForm.vue')
