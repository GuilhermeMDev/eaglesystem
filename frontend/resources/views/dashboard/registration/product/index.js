export { domain } from 'source/domains/Registration/Product/settings'

/** @type {string} */
export const path = '/dashboard/registration/product'

/** @type {string} */
export const icon = 'inventory_2'

/** @type {function} */
export const table = () => import('resources/views/dashboard/registration/product/ProductTable.vue')

/** @type {function} */
export const form = () => import('resources/views/dashboard/registration/product/ProductForm.vue')
