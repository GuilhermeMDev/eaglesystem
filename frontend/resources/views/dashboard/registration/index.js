/** @type {string} */
export { domain } from 'source/domains/Registration/settings'

/** @type {string} */
export const icon = 'star'
