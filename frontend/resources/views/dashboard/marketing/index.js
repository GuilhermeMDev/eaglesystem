/** @type {string} */
export { domain } from 'source/domains/Marketing/settings'

/** @type {string} */
export const icon = 'emoji_objects'
