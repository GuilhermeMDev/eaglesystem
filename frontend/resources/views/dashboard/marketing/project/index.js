export { domain } from 'source/domains/Marketing/Project/settings'

/** @type {string} */
export const path = '/dashboard/marketing/project'

/** @type {string} */
export const icon = 'schema'

/** @type {function} */
export const table = () => import('resources/views/dashboard/marketing/project/ProjectTable.vue')

/** @type {function} */
export const form = () => import('resources/views/dashboard/marketing/project/ProjectForm.vue')
