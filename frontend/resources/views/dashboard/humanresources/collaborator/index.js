export { domain } from 'source/domains/Humanresources/Collaborator/settings'

/** @type {string} */
export const path = '/dashboard/humanresources/collaborator'

/** @type {string} */
export const icon = 'badge'

/** @type {function} */
export const table = () => import('resources/views/dashboard/humanresources/collaborator/CollaboratorTable.vue')

/** @type {function} */
export const form = () => import('resources/views/dashboard/humanresources/collaborator/CollaboratorForm.vue')
