// add children
import * as project from 'resources/views/dashboard/marketing/project'

/**
* @param {AppRouterGroup} $router
*/
export default ($router) => {
  $router.provide(project)
}
