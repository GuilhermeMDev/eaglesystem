// add children
import * as provider from 'resources/views/dashboard/inventory/provider'

/**
* @param {AppRouterGroup} $router
*/
export default ($router) => {
  $router.provide(provider)
}
