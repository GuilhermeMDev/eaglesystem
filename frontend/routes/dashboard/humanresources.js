// add children
import * as collaborator from 'resources/views/dashboard/humanresources/collaborator'

/**
* @param {AppRouterGroup} $router
*/
export default ($router) => {
  $router.provide(collaborator)
}
