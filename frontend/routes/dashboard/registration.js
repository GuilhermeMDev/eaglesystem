// add children
import * as product from 'resources/views/dashboard/registration/product'

/**
* @param {AppRouterGroup} $router
*/
export default ($router) => {
  $router.provide(product)
}
